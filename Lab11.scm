;OR
(define OR
	(lambda (M N)
              (lambda(a b)
		(N a (M a b)))))
;AND
(define AND 
	(lambda (m n) 
		(lambda (a b) 
			(n (m a b) b))))

;LEQ
(define LEQ
	(lambda (m n) 
		(IsZero ( subtraction m n) )))
;add 
(define add 
	(lambda (m n s z)
              (m s(n s z))))
;LessThan
(define LessThan
	(lambda (a b)
		((AND( (NOT(EQ a b)) (LEQ a b) )))))
;isZero
(define isZero
	(lambda (n)
		(n (lambda(x) (false)) true)))

;define func
(define func
	(lambda (n f)
		((isZero n) n (add n f(n-1)))))

;define Y-combinator
(define Y-comb
	(lambda (f)
		( (lambda(x)f(x x)) (lambda(x)f(x x)) )))

;define if-else
(define if-else_func
	(lambda (m n)
		(OR((isZero m) (LessThan m n))
			(Y-comb func(n func))
			(add n m))))

